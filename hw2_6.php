<?php
/*
6. С помощью рекурсии организовать функцию возведения числа в степень. Формат: function
power($val, $pow), где $val – заданное число, $pow – степень.
*/
function power($val, $pow)
{
return $val ** $pow;
}
$val = 2;
$pow = 4;
$result = power($val, $pow);
echo "Число: " . $val . " в степени: " . $pow . "<br> = " . $result;


//----------------------------------------------
/*function factorial($x) {
    if ($x === 0) return 1; //if x=0
    else return $x*factorial($x-1);
}
echo factorial(4);
*/
function vozvstepen($param, $stepen) {
    if ($stepen === 0) return 1;
    else return $param*vozvstepen($param,$stepen-1);
}
echo "<br>" . vozvstepen(4,2);