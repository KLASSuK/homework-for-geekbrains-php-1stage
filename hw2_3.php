<?php
/*
3. Реализовать основные 4 арифметические операции в виде функций с двумя параметрами.
Обязательно использовать оператор return.
*/
$a = 1;
$b = 2;

function plus($a, $b)
{
    return ($a + $b);
}
$summa = plus(1, 2);
echo $summa ;

function minus($a, $b)
{
    return ($a - $b);
}
$raznost = minus(1, 2);
echo "<br>" . $raznost;

function delenie($a, $b)
{
    if ($b !=0) {
        return ($a / $b);
    } else {
        echo "<br> деление на 0 невозможно.";
    }
}
$delen = delenie(2, 0);
echo "<br>" . $delen;

function umnogenie($a, $b)
{
    return ($a * $b);
}
$umnog = umnogenie(1, 2);
echo "<br>" . $umnog;
