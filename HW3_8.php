<?php
/*
8. *Повторить третье задание, но вывести на экран только города, начинающиеся с буквы «К».
 */
//задание выполнил с символом первым "А".

$Russia = [
    'Московская область' => [
        'Абрамовка',
        'Авдеево',
        'Алабино',
        'Барвиха',
        'Голицыно',
        'Горловка',
        'Домодедово',
        'Мареч',
    ],
    'Ленинградская область' => [
        'Андреево',
        'Бабино',
        'Боровое',
        'Васьково',
        'Винницы',
        'Гаврилово',
        'Горка',
        'Елизаветино',
        'МарF',
    ],
    'Тюменская область' => [
        'Абалак',
        'Антипино',
        'Бердюжье',
        'Бизино',
        'Винзили',
        'Заречный',
        '1Маречный',
    ],
];
$count = count($Russia);//количество элементов в массиве
//var_dump($count); die;
$RussiaKeys = array_keys($Russia);// взяли ключи массива и сделали из него другой массив вида
//var_dump($mapKeys);
$values = array_values($Russia); //получили массив городов без обозначений областей
for ($i = 0 ; $i < $count; $i++) {
    $name = $RussiaKeys[$i]; //взяли название по ключу -> "a"
    $arr = array_filter($values[$i], function($k){
        return (mb_substr($k, 0, 1) == "А");
    });
    if($arr) {
        echo $name . "<br>";
        // var_dump($values[$i][0]); die;
        echo implode(', ', $arr) . "<br>";
    }
}